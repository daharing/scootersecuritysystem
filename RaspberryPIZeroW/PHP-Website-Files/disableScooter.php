<?php
//initialize the session
session_start();

// Check if logged in. Else redirect to login
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true ){
    header("location: login.php");
    exit;
}

//else we are logged in
else{

   //Run gpio command to set pin 18 to output and to disable (turn off) pin 18
   $command = escapeshellcmd('gpio -1 mode 18 output'); //make sure pin is in output mode
   $command = escapeshellcmd('gpio -1 write 18 0'); //set pin output to off

   $output = shell_exec($command);
   header("location: welcome.php");
   exit;
}
?>
