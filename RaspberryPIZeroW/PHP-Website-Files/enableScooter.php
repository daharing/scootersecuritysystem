<?php
//initialize the session
session_start();

// Check if logged in. Else redirect to login
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true ){
    header("location: login.php");
    exit;
}

//else we are logged in
else{

    //Run gpio command to set pin 18 to output and to enable (turn on) pin 18
    $command = escapeshellcmd('gpio -1 mode 18 output');//make sure pin is in output mode
    $command = escapeshellcmd('gpio -1 write 18 1'); //set pin output to on

    $output = shell_exec($command);

    //redirect to welcome page
    header("location: welcome.php");
    exit;
}
?>
